﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;

namespace MejorAutoMatching
{
    /// <summary>
    /// 申請書の状態を表すフラグ
    /// </summary>
    [Flags]
    public enum STATUS_FLAG
    {
        未処理 = 0x0,
        入力済 = 0x1, ベリファイ済 = 0x2, 自動マッチ済 = 0x4, 入力時エラー = 0x8,
        点検対象 = 0x10, 点検済 = 0x20, 返戻 = 0x40, 支払保留 = 0x80,
        照会対象 = 0x100, 保留 = 0x200, 過誤 = 0x400, 再審査 = 0x800,
        往療点検対象 = 0x1000, 往療疑義 = 0x2000, /*往療疑義なし = 0x4000,*/ 往療点検済 = 0x8000,
        追加入力済 = 0x1_0000, 追加ベリファイ済 = 0x2_0000, 拡張入力済 = 0x4_0000, 拡張ベリ済 = 0x8_0000,
        架電対象 = 0x10_0000, 支払済 = 0x20_0000, 処理1 = 0x40_0000, 処理2 = 0x80_0000,
    }


    class AppOcr
    {
        public const int SCAN_OCR_FINSHED = 3;
        public const int OCR_ORDERD = 3;
        public const int OCR_NOT_ORDERD = 4;

        public int AID { get; set; } = 0;
        public string Note2 { get; set; } = string.Empty;
        private string OcrData { get; set; }
        private string[] ocrs => OcrData.Split(',');
        public string InsNum { get; set; } = string.Empty;

        public string Num { get; set; } = string.Empty;
        public int MediYM { get; set; } = 0;
        public string DrNumber { get; set; } = string.Empty;
        public int Total { get; set; } = 0;
        public int AppType { get; set; } = 0;
        public int RrID { get; set; } = 0;

        /// <summary>
        /// 入力を行っていないApplicationを取得します
        /// </summary>
        /// <param name="scanID"></param>
        /// <returns></returns>
        public static List<AppOcr> GetOcr(DB db, int scanID)
        {
            var sql = "SELECT " +
                "a.aid, a.ocrdata, s.note2 FROM application AS a, scan AS s " +
                "WHERE a.scanid=:sid " +
                "AND a.scanid=s.sid " +
                $"AND a.statusflags<>(a.statusflags|{(int)STATUS_FLAG.入力済});";

            var p = new Dapper.DynamicParameters();
            p.Add("sid", scanID, System.Data.DbType.Int32);


            var l = db.Query<AppOcr>(sql, p).ToList();


            Log.WriteInfoLog("GetOcr,抽出" + l.Count);

            foreach (var item in l)
            {
                item.getPropertiesFromOcr();
            }

            Log.WriteInfoLog("GetOcr end");
            return l;
        }

        private void getPropertiesFromOcr()
        {
            AppType = Note2 == "7" ? 7 : Note2 == "8" ? 8 : 0;

            Log.WriteInfoLog("getPropertiesFromOcr,申請書タイプ Apptype:" + AppType);

            if (AppType == 0)//柔整
            {

                if (ocrs.Length < 195)
                {
                    Log.WriteInfoLog($"getPropertiesFromOcr,OCR文字列長さ:{ocrs.Length}");
                    return;
                }

                InsNum = ocrs[6];
                Num = ocrs[7];
                int temp;
                int.TryParse(ocrs[193], out temp);
                Total = temp;

                Log.WriteInfoLog($"getPropertiesFromOcr,申請書タイプ Apptype:柔整 InsNum:{InsNum} Num:{Num} Total:{Total}");
                //Log.WriteInfoLog($"getPropertiesFromOcr,申請書タイプ Apptype:{AppType} InsNum:{InsNum} Num:{Num} Total:{Total}");
            }
            else//あはき
            {
                Regex re = new Regex(@"[^0-9]");


                if (ocrs.Length < 40) return;
                InsNum = re.Replace(ocrs[5], "");
                Num = re.Replace(ocrs[6], "");
                DrNumber = "";



                int temp;
                var strTemp = re.Replace(ocrs[36], "");
                int.TryParse(strTemp, out temp);
                Total = temp;


                //年月
                int ypos = 0;
                int iY, iM;
                //不要な空白を除去
                string tY = ocrs[3].ToString().Trim();
                //全角を半角に変換
                tY = Strings.StrConv(tY, VbStrConv.Narrow);
                string tM = tY;


                Log.WriteInfoLog($"getPropertiesFromOcr,申請書タイプ Apptype:{AppType} InsNum:{InsNum} Num:{Num} Total:{Total}");


                ypos = tY.IndexOf("年");
                if (ypos > 0)
                {
                    //フォーマットファイルによっては年月が一つになっているため
                    if (ypos > 2)
                    {
                        tY = tY.Substring(ypos - 2, 2);
                    }
                    else
                    {
                        tY = tY.Substring(0, ypos);
                    }
                    tY = re.Replace(tY, "");
                    int.TryParse(tY, out iY);

                    if (tM.Length > ypos + 2)
                    {
                        tM = tM.Substring(ypos + 1, 2);
                    }
                    else
                    {
                        tM = tM.Substring(ypos + 1, tM.Length - ypos - 1);
                    }
                    tM = re.Replace(tM, "");
                    int.TryParse(tM, out iM);

                    Log.WriteInfoLog("getPropertiesFromOcr,年月ひとつ令和前 AID:" + this.AID + " iY=" + iY + " iM=" + iM);

                    //20190619115452 furukawa st ////////////////////////
                    //令和対応にて関数置換
                    if (iY>0 && iM > 0)
                    {
                        MediYM = (iY < 1 || 64 < iY) ? 0 : DateTimeEx.GetAdYearFromWarekiYM(iY, iM) * 100 + iM;
                    }
                    //MediYM = (iY < 1 || 60 < iY) ? 0 : DateTimeEx.GetAdYeaFromH(iY) * 100 + iM;

                    //20190619115452 furukawa ed ////////////////////////

                    Log.WriteInfoLog($"getPropertiesFromOcr,年月ひとつ令和後 MediYM={MediYM}");
                }

                return;
            }

            Log.WriteInfoLog("getPropertiesFromOcr,施術師番号整理前");

            //施術師番号整理
            var dn = ocrs[214].Trim();
            dn = dn.Replace("--", "-");
            int index = -1;
            if (dn.Length > 10)
            {
                for (int i = 7; i < dn.Length - 2; i++)
                {
                    if (dn[i] == '-' && "0123456789".Contains(dn[i + 1]) && dn[i + 2] == '-')
                    {
                        index = i;
                        break;
                    }
                }

                if (index != -1)
                {
                    dn = dn.Substring(index - 7);
                    dn = dn.Replace("-", "");
                    if (dn.Length > 9) dn = dn.Remove(9);
                }
            }
            DrNumber = dn;


            


            //診療年月
            const string numStr = "0123456789";

            //数字を抽出するDictionary
            var countDic = new Dictionary<int, int>();

            //和暦年
            int todayY = DateTimeEx.GetJpYear(DateTime.Today);
            
            //数字だけを抽出
            var func = new Func<string, int>(s =>
            {
                string code = string.Empty;
                foreach (var c in s)
                {
                    if (!numStr.Contains(c)) continue;
                    code += c;
                }
                if (code == string.Empty || code.Length > 8) return -1;
                return int.Parse(code);
            });

            //診療年
            var actY = new Action<string>(s =>
            {                
                int ty = func(s);



                //20190627185919 furukawa st ////////////////////////
                //年制限外す                               
                //if (ty > todayY || ty + 2 < todayY) return;

                //20190627185919 furukawa ed ////////////////////////

                //20200218113021 furukawa st ////////////////////////
                //年が－１の場合数字として扱わない

                if (ty + 2 < todayY) return;
                //20200218113021 furukawa ed ////////////////////////





                //どの数字が何回出たかをcountDicに入れてる
                if (!countDic.ContainsKey(ty)) countDic.Add(ty, 0);
                countDic[ty]++;
            });

            
            //ocrdataをカンマで区切った文字列の要素

            actY(ocrs[47]);//施術終了年
            actY(ocrs[50]);//実日数
            actY(ocrs[64]);//施術終了年
            actY(ocrs[67]);//実日数
            actY(ocrs[81]);//施術終了年
            actY(ocrs[84]);//実日数




            //20200218113427 furukawa st ////////////////////////
            //デバッグ用出力
            
            System.Diagnostics.Debug.Print($"施術終了年47:{ocrs[47]} 実日数50:{ocrs[50]} 施術終了年64:{ocrs[64]} 実日数67:{ocrs[67]} 施術終了年81:{ocrs[81]} 実日数84:{ocrs[84]}");
            //20200218113427 furukawa ed ////////////////////////




            int yc = 0;//数字のカウント
            int y = 0;//数字自体
            foreach (var item in countDic)
            {
                //countDicの中で最大カウントの数字をyに取得している
                if (item.Value > yc)
                {
                    yc = item.Value;
                    y = item.Key;
                }
            }


            //20200218113452 furukawa st ////////////////////////
            //デバッグ用出力
            
            System.Diagnostics.Debug.Print($"yc(カウント)={yc} y(数字)={y}");
            //20200218113452 furukawa ed ////////////////////////



            Log.WriteInfoLog("getPropertiesFromOcr,診療年後");

            //診療月
            var actM = new Action<string>(s =>
            {
                int tm = func(s);
                if (tm < 0) return;
                if (tm > 12)
                {
                    // ・を9に読み違えることが多い
                    if (s[0] == '9')
                    {
                        tm = func(s.Substring(1));
                    }
                    else if (s[s.Length - 1] == '9')
                    {
                        tm = func(s.Remove(s.Length - 1));
                    }
                    else
                    {
                        tm = func(s.Substring(1));
                        if (tm > 12 || tm < 0) tm = func(s.Remove(1));
                    }
                    if (tm > 12 || tm < 0) return;
                }
                if (!countDic.ContainsKey(tm)) countDic.Add(tm, 0);
                countDic[tm]++;
            });

            Log.WriteInfoLog("getPropertiesFromOcr,診療月後");

            countDic.Clear();
            actM(ocrs[48]);//施術月
            actM(ocrs[51]);//治癒
            actM(ocrs[65]);//施術月
            actM(ocrs[68]);//治癒
            actM(ocrs[82]);//施術月
            actM(ocrs[85]);//治癒




            //20200218113507 furukawa st ////////////////////////
            //デバッグ用出力
            
            System.Diagnostics.Debug.Print($"施術月48:{ocrs[48]}  治癒51:{ocrs[51]}  施術月65:{ocrs[65]}  治癒68:{ocrs[68]}  施術月82:{ocrs[82]}  治癒85:{ocrs[85]}");
            //20200218113507 furukawa ed ////////////////////////


            int mc = 0;
            int m = 0;
            foreach (var item in countDic)
            {
                if (item.Value > mc)
                {
                    mc = item.Value;
                    m = item.Key;
                }
            }

            Log.WriteInfoLog("getPropertiesFromOcr,令和前 AID:" + this.AID + " y=" + y + " m=" + m);


            //20190626140723 furukawa st ////////////////////////
            //月０対応
            
    
            if (y>0 && m>0)
            {
                MediYM = DateTimeEx.GetAdYearFromWarekiYM(y, m) * 100 + m;
            }
            else
            {
                MediYM = 0;
            }

            //20190619120432 furukawa st ////////////////////////
            //令和対応にて関数置換

            //MediYM = y > 0 && m > 0 ? DateTimeEx.GetAdYearFromWarekiYM(y, m) * 100 + m : 0;

            //MediYM = y > 0 ? DateTimeEx.GetAdYeaFromH(y) * 100 + m : 0;
            //20190619120432 furukawa ed ////////////////////////
            
            
            //20190626140723 furukawa ed ////////////////////////

            Log.WriteInfoLog("getPropertiesFromOcr,令和後");
        }
    }
}
