﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MejorAutoMatching
{
    class HiroshimaKoiki : Insurer
    {
        class Mapper
        {
            public int RrID { get; set; }
            public int CYM { get; set; }
            public string MYM { get; set; }
            public string Num { get; set; }
            public int Total { get; set; }

            public HiroshimaKoikiData ToProvidedData()
            {
                var hkd = new HiroshimaKoikiData();
                hkd.RrID = RrID;
                hkd.ChargeYM = CYM;
                hkd.MediYM = int.Parse(MYM);
                hkd.Num = Num;
                hkd.Total = Total;
                return hkd;
            }
        }
        
        class HiroshimaKoikiData : ReferData
        {
            public override bool SetAid(DB db, int aid, DB.Transaction tran)
            {
                var sql = "UPDATE refrece SET aid=@aid WHERE rrid=@rrid;";
                return db.Excute(sql, new { aid = aid, rrid = RrID });
            }
        }

        protected override ReferData getMatchData(AppOcr app)
        {
            var l = new List<ReferData>();

            for (int i = 0; i <= app.Num.Length - 8; i++)
            {
                var n = app.Num.Substring(i, 8);
                if (dic.ContainsKey(n)) l.AddRange(dic[n]);
            }

            l = l.FindAll(r => r.MediYM == app.MediYM && r.Total == app.Total);

            if (l.Count == 1) return l[0];
            return null;
        }

        protected override bool getProvidedDatas()
        {
            dic.Clear();

            try
            {
                var sql = "SELECT rrid, mym, num, total " +
                    "FROM refrece WHERE cym=@cym";

                var l = db.Query<Mapper>(sql, new { cym = targetCYM });
                foreach (var item in l)
                {
                    var pd = item.ToProvidedData();
                    if (!dic.ContainsKey(pd.Num)) dic.Add(pd.Num, new List<ReferData>());
                    dic[pd.Num].Add(pd);
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.ErrorWrite(ex);
                return false;
            }

        }
    }
}
