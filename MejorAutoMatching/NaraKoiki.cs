﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MejorAutoMatching
{
    class NaraKoiki : Insurer
    {
        class NaraKoikiData : ReferData
        {
            public override bool SetAid(DB db, int aid, DB.Transaction tran)
            {
                var sql = "UPDATE koikidata SET aid=@aid WHERE did=@did;";
                return db.Excute(sql, new { aid = aid, did = RrID });
            }
        }

        protected override ReferData getMatchData(AppOcr app)
        {
            var l = new List<ReferData>();

            for (int i = 0; i <= app.Num.Length - 8; i++)
            {
                var n = app.Num.Substring(i, 8);
                if (dic.ContainsKey(n)) l.AddRange(dic[n]);
            }

            l = l.FindAll(r => r.MediYM == app.MediYM && r.Total == app.Total);

            if (l.Count == 1) return l[0];
            return null;
        }

        protected override bool getProvidedDatas()
        {
            dic.Clear();

            try
            {
                var sql = "SELECT did, mediym, num, drnum, total " +
                    "FROM koikidata WHERE appym=@appym";

                var l = db.Query<NaraKoikiData>(sql, new { appym = targetCYM });
                foreach (var item in l)
                {
                    if (!dic.ContainsKey(item.Num)) dic.Add(item.Num, new List<ReferData>());
                    dic[item.Num].Add(item);
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.ErrorWrite(ex);
                return false;
            }

        }
    }
}
