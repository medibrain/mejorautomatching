﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MejorAutoMatching
{
    class OsakaKoiki : Insurer
    {
        class Mapper
        {
            public int RrID { get; set; }
            public int CYM { get; set; }
            public string MYM { get; set; }
            public string Num { get; set; }
            public int Total { get; set; }

            public OsakaKoikiData ToProvidedData()
            {
                var okd = new OsakaKoikiData();
                okd.RrID = RrID;
                okd.ChargeYM = CYM;
                okd.MediYM = int.Parse(MYM);
                okd.Num = Num;
                okd.Total = Total;
                return okd;
            }
        }

        class OsakaKoikiData : ReferData
        {
            public override bool SetAid(DB db, int aid, DB.Transaction tran)
            {
                var sql = "UPDATE refrece SET aid=@aid WHERE rrid=@rrid;";
                return db.Excute(sql, new { aid = aid, rrid = RrID });
            }
        }
        
        protected override ReferData getMatchData(AppOcr app)
        {
            var l = new List<ReferData>();
            //提供データ取得
            for (int i = 0; i <= app.Num.Length - 8; i++)
            {
                var n = app.Num.Substring(i, 8);
                if (dic.ContainsKey(n)) l.AddRange(dic[n]);
            }

            //refreceとapplicationを診療年月、金額で取得している
            l = l.FindAll(r => r.MediYM == app.MediYM && r.Total == app.Total);

            //上でrefreceとapplicationを診療年月、金額で取得し、１件以外の場合はマッチング無しとなっている
            //どのレコードが正しいか判別できないと思われる
            if (l.Count == 1) return l[0];

            Log.WriteInfoLog($"getmatchdatra osaka_koiki mediYM:{app.MediYM} Total:{app.Total}");

            return null;
        }

        protected override bool getProvidedDatas()
        {
            dic.Clear();

            try
            {
                var sql = "SELECT rrid, mym, num, total " +
                    "FROM refrece WHERE cym=@cym";

                var l = db.Query<Mapper>(sql, new { cym = targetCYM });
                foreach (var item in l)
                {
                    var pd = item.ToProvidedData();
                    if (!dic.ContainsKey(pd.Num)) dic.Add(pd.Num, new List<ReferData>());
                   dic[pd.Num].Add(pd);
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.ErrorWrite(ex);
                return false;
            }
        }
    }
}
