﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MejorAutoMatching
{
    public partial class MainForm : Form
    {
        public bool Cancel = false;
        List<Insurer> insurers;
        BindingSource bs = new BindingSource();

        public MainForm()
        {
            //20190903092404 furukawa st ////////////////////////
            //本番サーバ以外の時は背景色ピンクに
            
            if (Settings.DBAddress != "192.168.200.100")
                this.BackColor=System.Drawing.Color.Thistle;
            else
                this.BackColor = System.Drawing.SystemColors.Control;

            //20190903092404 furukawa ed ////////////////////////


            InitializeComponent();
            insurers = insurers = Insurer.LoadInsurers(this);
            bs.DataSource = insurers;
            dataGridView1.DataSource = bs;

            dataGridView1.Columns["Name"].HeaderText = "保険者名";
            dataGridView1.Columns["DbName"].Visible = false;
            dataGridView1.Columns["TargetHYM"].HeaderText = "監視月";
            dataGridView1.Columns["TargetHYM"].Width = 80;
            dataGridView1.Columns["StartDT"].HeaderText = "監視開始日時";
            
            //20200218114625 furukawa st ////////////////////////
            //文字サイズに合わせて幅調整
            dataGridView1.Columns["StartDT"].Width = 170;
            //dataGridView1.Columns["StartDT"].Width = 140;
            //20200218114625 furukawa ed ////////////////////////

            dataGridView1.Columns["LastCheckDT"].HeaderText = "最終チェック";
            //20200218114706 furukawa st ////////////////////////
            //文字サイズに合わせて幅調整
            dataGridView1.Columns["LastCheckDT"].Width = 170;
            //dataGridView1.Columns["LastCheckDT"].Width = 140;
            //20200218114706 furukawa ed ////////////////////////


            //20200221122213 furukawa st ////////////////////////
            //サーバIP、ポートを表示
            
            dataGridView1.Columns["ServerIP"].HeaderText = "サーバIP";
            dataGridView1.Columns["ServerIP"].Width = 140;
            dataGridView1.Columns["DBPort"].HeaderText = "ﾎﾟｰﾄ";
            dataGridView1.Columns["DBPort"].Width = 50;
            //20200221122213 furukawa ed ////////////////////////          


            DataGridViewCellStyle cs = new DataGridViewCellStyle();
            cs.SelectionBackColor = Color.FromArgb(183, 219, 255);
            cs.SelectionForeColor = Color.Black;
            dataGridView1.DefaultCellStyle = cs;

            //20200221144948 furukawa st ////////////////////////
            //DBuser、password非表示
            
            dataGridView1.Columns["DBUser"].Visible = false;
            dataGridView1.Columns["DBPass"].Visible = false;
            //20200221144948 furukawa ed ////////////////////////

            timer1.Start();
        }

        public void LogPrint(string str)
        {
            var fn = Application.StartupPath + "\\Info\\" + DateTime.Today.ToString("yyyyMM") + ".log";
            str = DateTime.Now.ToString() + "   " + str;
            using (var sw = new System.IO.StreamWriter(fn, true, Encoding.GetEncoding("Shift_JIS")))
            {
                sw.WriteLine(str);
            }

            Invoke(new Action(() =>
            {
                var s = textBox1.Text;
                if (s.Length > 100000)
                {
                    textBox1.Text = s.Substring(s.IndexOf('\n') + 1);
                }

                //20200217151240 furukawa st ////////////////////////
                //進行表示設定に合わせて変更
                
                        //textBox1.AppendText(str + "\r\n");

                if (Settings.LogSort == 0) textBox1.Text += str + "\r\n";
                if (Settings.LogSort == 1) textBox1.Text = str + "\r\n" + textBox1.Text;

                //20200217151240 furukawa ed ////////////////////////
            }));
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            buttonStart.Enabled = false;
            buttonStop.Enabled = true;
            var ins = ((Insurer)bs.Current);

            using (var f = new StartForm(this, ins)) f.ShowDialog();
            bs.ResetBindings(false);
        }
        
        private async void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                timer1.Stop();
                foreach (Insurer item in bs)
                {
                    await Task.Run(() => item.Matching(this));
                }
            }
            finally
            {
                bs.ResetBindings(false);
                timer1.Start();
            }
        }

        private void dataGridView1_CurrentCellChanged(object sender, EventArgs e)
        {
            var ins = ((Insurer)bs.Current);
            if (ins.TargetHYM == null)
            {
                buttonStart.Enabled = true;
                buttonStop.Enabled = false;
            }
            else
            {
                buttonStart.Enabled = false;
                buttonStop.Enabled = true;
            }
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            var ins = ((Insurer)bs.Current);
            ins.Stop(this);
            bs.ResetBindings(false);
        }
    }
}
