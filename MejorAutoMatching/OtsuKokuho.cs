﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MejorAutoMatching
{
    class OtsuKokuho : Insurer
    {
        class Mapper
        {
            public int RrID { get; set; }
            public int CYM { get; set; }
            public string YM { get; set; }
            public string Num { get; set; }
            public int Total { get; set; }

            public OtsuKokuhoData ToProvidedData()
            {
                var okd = new OtsuKokuhoData();
                okd.RrID = RrID;
                okd.ChargeYM = CYM;
                okd.MediYM = int.Parse(YM);
                okd.Num = Num;
                okd.Total = Total;
                return okd;
            }
        }
        
        class OtsuKokuhoData : ReferData
        {
            public override bool SetAid(DB db, int aid, DB.Transaction tran)
            {
                var sql = "UPDATE refrece SET aid=@aid WHERE rrid=@rrid;";
                return db.Excute(sql, new { aid = aid, rrid = RrID });
            }
        }

        protected override ReferData getMatchData(AppOcr app)
        {
            var l = new List<ReferData>();

            for (int i = 0; i <= app.Num.Length - 7; i++)
            {
                var n = app.Num.Substring(i, 7);
                if (dic.ContainsKey(n)) l.AddRange(dic[n]);
            }

            l = l.FindAll(r => r.MediYM == app.MediYM && r.Total == app.Total);

            if (l.Count == 1) return l[0];
            return null;
        }

        protected override bool getProvidedDatas()
        {
            dic.Clear();

            try
            {
                var sql = "SELECT rrid, ym, num, total " +
                    "FROM refrece WHERE cym=@cym";

                var l = db.Query<Mapper>(sql, new { cym = targetCYM });
                foreach (var item in l)
                {
                    var pd = item.ToProvidedData();
                    if (!dic.ContainsKey(pd.Num)) dic.Add(pd.Num, new List<ReferData>());
                    dic[pd.Num].Add(pd);
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.ErrorWrite(ex);
                return false;
            }

        }
    }
}
