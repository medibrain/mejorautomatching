﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MejorAutoMatching
{
    abstract class ReferData
    {
        public int RrID { get; set; }
        public int ChargeYM { get; set; }
        public int MediYM { get; set; }
        public string Num { get; set; }
        public string DrNum { get; set; }
        public int Total { get; set; }

        /// <summary>
        /// 提供データに関連付けたAIDを登録します
        /// </summary>
        /// <param name="db"></param>
        /// <param name="aid"></param>
        /// <param name="tran"></param>
        /// <returns></returns>
        public abstract bool SetAid(DB db, int aid, DB.Transaction tran);
    }
}
