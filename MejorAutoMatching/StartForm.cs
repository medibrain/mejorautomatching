﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MejorAutoMatching
{
    public partial class StartForm : Form
    {
        Insurer ins;
        MainForm mf;

        internal StartForm(MainForm mf, Insurer ins)
        {
            InitializeComponent();
            this.ins = ins;
            this.mf = mf;
            this.Text = ins.Name + "OCR監視開始設定";

            lblinsname.Text = ins.Name;


            //20190620094113 furukawa st ////////////////////////
            //西暦入力とする
            textBox1.Text = DateTime.Today.Year.ToString();
                                //textBox1.Text = DateTimeEx.GetHYear(DateTime.Today.Year).ToString();
            //20190620094113 furukawa ed ////////////////////////

            textBox2.Text = DateTime.Today.Month.ToString();
        }

        private async void buttonStart_Click(object sender, EventArgs e)
        {
            int hy, m;
            int.TryParse(textBox1.Text, out hy);
            int.TryParse(textBox2.Text, out m);

            if (hy == 0 || m == 0)
            {
                return;
            }


            //20190709153243 furukawa st ////////////////////////
            //ログ数制限            
            Log.limitlogCount();
            //20190709153243 furukawa ed ////////////////////////



            //20190620094316 furukawa st ////////////////////////
            //西暦入力とする
            int ym = hy;
                                //int ym = DateTimeEx.GetAdYeaFromH(hy);
            //20190620094316 furukawa ed ////////////////////////

            ym = ym * 100 + m;
            
            this.Close();

            //20200221134705 furukawa st ////////////////////////
            //保険者別接続先を管理するため、保険者クラスを使用する
            
            await Task.Run(() => ins.Start(ym, mf,ins));
            //await Task.Run(() => ins.Start(ym, mf));

            //20200221134705 furukawa ed ////////////////////////
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
