﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Dapper;
using Npgsql;

namespace MejorAutoMatching
{
    class DB
    {
        public static DB Main { get; private set; }
        //static DB()
        //{
        //    Main = new DB("localhost", 5434, "jyusei", "postgres", "pass");
        //    //Main = new DB("localhost", 5432, "jyusei", "postgres", "pass");
        //}

        string conStr = string.Empty;

        public DB(string host, int port, string database, string userName, string pass)
        {
            var sb = new NpgsqlConnectionStringBuilder();
            sb.Host = host;
            sb.Port = port;
            sb.Database = database;
            sb.Username = userName;
            sb.Password = pass;
            sb.Timeout = 60;
            sb.ClientEncoding = "UNICODE";

            conStr = sb.ToString();
        }

        private NpgsqlConnection getOpenConnection()
        {
            NpgsqlConnection con = null;

            try
            {
                con = new NpgsqlConnection(conStr);
                con.Open();
            }
            catch
            {
                try
                {
                    if (con != null) con.Dispose();
                    con = null;
                    con = new NpgsqlConnection(conStr);
                    con.Open();
                }
                catch (Exception ex)
                {
                    Log.ErrorWriteAndExit(ex);
                    throw ex;
                }
            }
            return con;
        }

        public class Transaction : IDisposable
        {
            bool disposed = false;
            public NpgsqlTransaction tran { get; private set; }
            public NpgsqlConnection con { get; private set; }

            public Transaction(DB db)
            {
                con = db.getOpenConnection();
                tran = con.BeginTransaction();
            }

            ~Transaction()
            {
                Dispose();
            }

            public void Dispose()
            {
                if (!tran.IsCompleted) tran.Rollback();
                if (!disposed) con.Dispose();
                con = null;
                disposed = true;
            }

            public void Commit()
            {
                tran.Commit();
            }

            public void Rollback()
            {
                tran.Rollback();
            }
        }

        public Transaction CreateTran()
        {
            var tran = new Transaction(this);
            return tran;
        }

        public IEnumerable<Type> Query<Type>(string sql, object ps)
        {
            try
            {
                using (var con = getOpenConnection())
                {
                    var res = con.Query<Type>(sql, ps);
                    return res;
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return null;
            }
        }

        public bool Excute(string sql, object obj)
        {
            try
            {
                using (var con = getOpenConnection())
                {
                    con.Execute(sql, obj);
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return false;
            }
            return true;
        }

        public bool Excute(string sql, object obj, Transaction tran)
        {
            try
            {
                tran.con.Execute(sql, obj, tran.tran);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
                return false;
            }
            return true;
        }
    }
}
