﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;

static class DateTimeEx
{
    public static DateTime DateTimeNull = DateTime.MinValue;
    public static string ErrorMessage = string.Empty;
    public static string EraString { get; private set; }
    static List<EraJ> EraList = new List<EraJ>();
    public static System.Globalization.CultureInfo culture;

    static DateTimeEx()
    {
        culture = new System.Globalization.CultureInfo("ja-JP", true);
        culture.DateTimeFormat.Calendar = new System.Globalization.JapaneseCalendar();
        GetEraList();
    }

    internal class EraJ
    {
        internal DateTime SDate = DateTime.MaxValue;
        internal DateTime EDate = DateTime.MaxValue;
        internal string Name = string.Empty;
        internal string ShortName = string.Empty;
        internal string Initial = string.Empty;
        internal int Number = 0;
        internal int Difference = 0;
    }

    /// <summary>
    /// 和暦年号一覧を指定します
    /// </summary>
    /// <returns></returns>
    private static void GetEraList()
    {
       


        var m = new EraJ();
        m.SDate = new DateTime(1868, 9, 8);
        m.EDate = new DateTime(1912, 7, 29);
        m.Name = "明治";
        m.ShortName = "明";
        m.Number = 1;
        m.Initial = "M";
        m.Difference = 1867;
        EraList.Add(m);

        var t = new EraJ();
        t.SDate = new DateTime(1912, 7, 30);
        t.EDate = new DateTime(1926, 12, 24);
        t.Name = "大正";
        t.ShortName = "大";
        t.Number = 2;
        t.Initial = "T";
        t.Difference = 1911;
        EraList.Add(t);

        var s = new EraJ();
        s.SDate = new DateTime(1926, 12, 25);
        s.EDate = new DateTime(1989, 1, 7);
        s.Name = "昭和";
        s.ShortName = "昭";
        s.Number = 3;
        s.Initial = "S";
        s.Difference = 1925;
        EraList.Add(s);

        var h = new EraJ();
        h.SDate = new DateTime(1989, 1, 8);
        
        //20190619110227 furukawa st ////////////////////////
        //平成最後の日
        h.EDate = new DateTime(2019, 4, 30);
        //h.EDate = DateTime.MaxValue;
        //20190619110227 furukawa ed ////////////////////////

        h.Name = "平成";
        h.ShortName = "平";
        h.Number = 4;
        h.Initial = "H";
        h.Difference = 1988;
        EraList.Add(h);


        //20190619101131 furukawa st ////////////////////////
        //令和対応
        
        var r = new EraJ();
        r.SDate = new DateTime(2019, 5, 1);
        r.EDate = DateTime.MaxValue;
        r.Name = "令和";
        r.ShortName = "令";
        r.Number = 5;
        r.Initial = "R";
        r.Difference = 2018;
        EraList.Add(r);
        //20190619101131 furukawa ed ////////////////////////




        //年号を表す文字列を整備
        foreach (var item in EraList)
        {
            EraString += item.ShortName;
            EraString += item.Initial;
            EraString += item.Initial.ToLower();
        }
    }


    /// <summary>
    /// 年号の略(英頭文字)を取得します。なかった場合、空白が返ります。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static string GetEraShort(DateTime dt)
    {
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate >= dt) continue;
            if (EraList[i].EDate <= dt) continue;
            return EraList[i].ShortName;
        }

        return string.Empty;
    }

    /// <summary>
    /// 年号を漢字正式名称で取得します
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static string GetEra(DateTime dt)
    {
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return EraList[i].Name;
        }

        return string.Empty;
    }

    /// <summary>
    /// 年号を表す数字(昭和=3、平成=4)を取得します。なかった場合、0が返ります。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static int GetEraNumber(DateTime dt)
    {
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate >= dt) continue;
            if (EraList[i].EDate <= dt) continue;
            return EraList[i].Number;
        }

        return 0;
    }

    /// <summary>
    /// 対応する和暦の年を返します。なかった場合、0が返ります。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static int GetJpYear(DateTime dt)
    {
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate >= dt) continue;
            if (EraList[i].EDate <= dt) continue;
            return dt.Year - EraList[i].Difference;
        }

        return 0;
    }

    /// <summary>
    /// 対応する和暦の年月を正式年号付きで返します。なかった場合、0が返ります。
    /// </summary>
    public static string GetEraJpYearMonth(DateTime dt)
    {
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate >= dt) continue;
            if (EraList[i].EDate <= dt) continue;
            return EraList[i].Name + (dt.Year - EraList[i].Difference).ToString("00年") + dt.ToString("MM月");
        }

        return string.Empty;
    }

    /// <summary>
    /// 対応する和暦の年を正式年号付きで返します。なかった場合、0が返ります。
    /// </summary>
    public static string GetEraJpYear(DateTime dt)
    {
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return EraList[i].Name + (dt.Year - EraList[i].Difference).ToString("00");
        }

        return string.Empty;
    }


    /// <summary>
    /// 対応する和暦の年を英頭文字付きで返します。なかった場合、0が返ります。
    /// </summary>
    public static string GetShortEraJpYear(DateTime dt)
    {
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return EraList[i].Initial + (dt.Year - EraList[i].Difference).ToString("00");
        }

        return string.Empty;
    }

    /// <summary>
    /// 対応する和暦の年を昭和=3とするJYY形式のint型で返します。
    /// 失敗した場合0が返ります。
    /// </summary>
    public static int GetEraNumberYear(DateTime dt)
    {
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate > dt) continue;
            if (EraList[i].EDate < dt) continue;
            return EraList[i].Number * 100 + (dt.Year - EraList[i].Difference);
        }

        return 0;
    }


    /// <summary>
    /// 年号(2文字)つきの和暦年から、西暦年を取得します。失敗した場合0が返ります。
    /// </summary>
    /// <param name="jyear">JJyy</param>
    /// <returns></returns>
    public static int GetAdYear(string jyear)
    {
        int year = 0;
        if (jyear.Length < 3) return 0;
        string era = jyear.Remove(2);
        if (!int.TryParse(jyear.Substring(2), out year)) return 0;

        int diff = 0;
        foreach (var item in EraList)
        {
            if (era.Contains(item.Initial) || era.Contains(item.Initial.ToLower()) || era.Contains(item.ShortName))
            {
                diff = item.Difference;
                break;
            }
        }
        if (diff == 0)
        {
            return 0;
        }

        year += diff;
        return year;
    }

    /// <summary>
    /// 昭和=3とした年号つきの和暦年月から、西暦年月を取得します。失敗した場合0が返ります。
    /// </summary>
    /// <param name="jyymm"></param>
    /// <returns></returns>
    public static int GetAdYearMonthFromJyymm(int jyymm)
    {
        //20190619104424 furukawa st ////////////////////////
        //令和対応でチェック範囲を拡大
        
        if (jyymm <= 10000 || 60000 <= jyymm) return 0;
                //if (jyymm <= 10000 || 50000 <= jyymm) return 0;
        //20190619104424 furukawa ed ////////////////////////



        int era = jyymm / 10000;
        int yymm = jyymm % 10000;

        int diff = 0;
        foreach (var item in EraList)
        {
            if (item.Number == era)
            {
                diff = item.Difference;
                break;
            }
        }

        if (diff == 0) return 0;
        return yymm += diff * 100;
    }

    /// <summary>
    /// 西暦年4ケタの文字列を和暦年に変換します。
    /// </summary>
    /// <param name="yearStr4"></param>
    /// <returns></returns>
    public static string GetJpYearStr(string yearStr4)
    {
        int year;
        int.TryParse(yearStr4, out year);
        if (year == 0) return string.Empty;
        var dt = new DateTime(year, 1, 1);
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate >= dt) continue;
            if (EraList[i].EDate <= dt) continue;
            return EraList[i].Name + (dt.Year - EraList[i].Difference).ToString("00");
        }
        return string.Empty;
    }

    /// <summary>
    /// 西暦年4ケタの文字列を和暦年に変換します。
    /// </summary>
    /// <param name="yearStr4"></param>
    /// <returns></returns>
    public static string GetJpYearStr(int year)
    {
        if (year == 0) return string.Empty;
        var dt = new DateTime(year, 1, 1);
        for (int i = 0; i < EraList.Count; i++)
        {
            if (EraList[i].SDate >= dt) continue;
            if (EraList[i].EDate <= dt) continue;
            return EraList[i].Name + (dt.Year - EraList[i].Difference).ToString("00");
        }
        return string.Empty;
    }

    /// <summary>
    /// 年号つきの和暦年から、西暦年を取得します。
    /// </summary>
    /// <param name="jyear">JJyy</param>
    /// <returns></returns>
    public static bool TryGetYear(string jyear, out int year)
    {
        year = 0;
        if (jyear.Length < 3) return false;
        string era = jyear.Remove(2);
        if (!int.TryParse(jyear.Substring(2), out year)) return false;

        int diff = 0;
        foreach (var item in EraList)
        {
            if (era.Contains(item.Initial) || era.Contains(item.Initial.ToLower()) || era.Contains(item.ShortName))
            {
                diff = item.Difference;
                break;
            }
        }
        if (diff == 0)
        {
            year = 0;
            return false;
        }

        year += diff;
        return true;
    }

    /// <summary>
    /// 文字列を日付に変換します。
    /// </summary>
    /// <param name="jDate"></param>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static bool TryGetYearMonthTimeFromJdate(string jDate, out DateTime dt)
    {
        dt = DateTimeNull;

        //アンダーラインはスペースに
        jDate = jDate.Replace('_', ' ');

        //年月日区切りをスラッシュに
        StringBuilder sb = new StringBuilder();
        const string separator = "年月・.";
        foreach (var c in jDate)
        {
            if (separator.Contains(c)) sb.Append('/');
            else sb.Append(c);
        }
        jDate = sb.ToString();
        sb.Clear();

        //半角に置き換え
        jDate = KanaToHalf(jDate);

        var dateStr = jDate.Split('/');

        //区切りなしの場合は調整
        if (dateStr.Length == 1)
        {
            dateStr = new string[2];
            if (jDate.Length == 6)
            {
                //西暦として扱う
                dateStr[0] = jDate.Remove(2);
                dateStr[1] = jDate.Substring(2, 2);
            }
            else if (jDate.Length == 7)
            {
                //和暦として扱う
                dateStr[0] = jDate.Remove(3);
                dateStr[1] = jDate.Substring(3, 2);
            }
            else if (jDate.Length == 8)
            {
                //西暦として扱う
                dateStr[0] = jDate.Remove(4);
                dateStr[1] = jDate.Substring(4, 2);
            }
            else return false;
        }

        if (dateStr.Length < 2) return false;

        //年の数字だけ取出し
        const string numStr = "0123456789";
        foreach (var c in dateStr[0]) if (numStr.Contains(c)) sb.Append(c);
        string yearNumStr = sb.ToString();

        //数字部分が3ケタ時、1文字目を年号に変換
        if (yearNumStr.Length == 3)
        {
            int eraNum;
            if (!int.TryParse(yearNumStr.Remove(1), out eraNum)) return false;
            foreach (var item in EraList)
            {
                if (item.Number == eraNum)
                {
                    dateStr[0] = item.Initial + yearNumStr.Substring(1);
                    yearNumStr = yearNumStr.Substring(1);
                    break;
                }
            }
        }

        int year, month;
        if (!int.TryParse(yearNumStr, out year)) return false;
        if (!int.TryParse(dateStr[1], out month)) return false;

        foreach (var item in EraList)
        {
            if (dateStr[0].Contains(item.Initial) || dateStr[0].Contains(item.Initial.ToLower()) || dateStr[0].Contains(item.ShortName))
            {
                year += item.Difference;
                break;
            }
        }

        if (!IsDate(year, month, 1)) return false;
        dt = new DateTime(year, month, 1);
        return true;
    }

    /// <summary>
    /// 昭和=3をはじめとする7ケタの数字文字列から日付を取得します
    /// 失敗した場合、DateTime.MinValueが返ります
    /// </summary>
    /// <param name="jdateInt7"></param>
    /// <returns></returns>
    public static DateTime GetDateFromJstr7(string jdate7)
    {
        int jdateInt7;
        if (!int.TryParse(jdate7, out jdateInt7)) return DateTime.MinValue;
        return GetDateFromJInt7(jdateInt7);
    }


    /// <summary>
    /// 昭和=3をはじめとする7ケタの数字から日付を取得します
    /// 失敗した場合、DateTime.MinValueが返ります
    /// </summary>
    /// <param name="jdateInt7"></param>
    /// <returns></returns>
    public static DateTime GetDateFromJInt7(int jdateInt7)
    {
        var eraNum = jdateInt7 / 1000000;
        var era = EraList.FirstOrDefault(e => e.Number == eraNum);
        if (era == null) return DateTime.MinValue;

        var y = jdateInt7 / 10000 % 100;
        y = y + era.Difference;
        var m = jdateInt7 / 100 % 100;
        var d = jdateInt7 % 100;

        return IsDate(y, m, d) ? new DateTime(y, m, d) : DateTime.MinValue;
    }

    /// <summary>
    /// 文字列を日付に変換します。
    /// </summary>
    /// <param name="jDate"></param>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static bool TryGetDateTimeFromJdate(string jDate, out DateTime dt)
    {
        dt = DateTimeNull;

        //アンダーラインはスペースに
        jDate = jDate.Replace('_', ' ');

        //年月日区切りをスラッシュに
        StringBuilder sb = new StringBuilder();
        const string separator = "年月日・.";
        foreach (var c in jDate)
        {
            if (separator.Contains(c)) sb.Append('/');
            else sb.Append(c);
        }
        jDate = sb.ToString();
        sb.Clear();

        //半角に置き換え
        jDate = KanaToHalf(jDate);

        var dateStr = jDate.Split('/');

        //区切りなしの場合は調整
        if (dateStr.Length == 1)
        {
            dateStr = new string[3];
            if (jDate.Length == 6)
            {
                //西暦として扱う
                dateStr[0] = jDate.Remove(2);
                dateStr[1] = jDate.Substring(2, 2);
                dateStr[2] = jDate.Substring(4, 2);
            }
            else if (jDate.Length == 7)
            {
                //和暦として扱う
                dateStr[0] = jDate.Remove(3);
                dateStr[1] = jDate.Substring(3, 2);
                dateStr[2] = jDate.Substring(5, 2);
            }
            else if (jDate.Length == 8)
            {
                //西暦として扱う
                dateStr[0] = jDate.Remove(4);
                dateStr[1] = jDate.Substring(4, 2);
                dateStr[2] = jDate.Substring(6, 2);
            }
            else return false;
        }

        if (dateStr.Length < 3) return false;

        //年の数字だけ取出し
        const string numStr = "0123456789";
        foreach (var c in dateStr[0]) if (numStr.Contains(c)) sb.Append(c);
        string yearNumStr = sb.ToString();

        //数字部分が3ケタ時、1文字目を年号に変換
        if (yearNumStr.Length == 3)
        {
            int eraNum;
            if (!int.TryParse(yearNumStr.Remove(1), out eraNum)) return false;
            foreach (var item in EraList)
            {
                if (item.Number == eraNum)
                {
                    dateStr[0] = item.Initial + yearNumStr.Substring(1);
                    yearNumStr = yearNumStr.Substring(1);
                    break;
                }
            }
        }

        int year, month, day;
        if (!int.TryParse(yearNumStr, out year)) return false;
        if (!int.TryParse(dateStr[1], out month)) return false;
        if (!int.TryParse(dateStr[2], out day)) return false;

        foreach (var item in EraList)
        {
            if (dateStr[0].Contains(item.Initial) || dateStr[0].Contains(item.Initial.ToLower()) || dateStr[0].Contains(item.ShortName))
            {
                year += item.Difference;
                break;
            }
        }

        if (!IsDate(year, month, day)) return false;
        dt = new DateTime(year, month, day);
        return true;
    }

    /// <summary>
    /// 正しい日付かどうかチェックします。
    /// </summary>
    public static bool IsDate(int iYear, int iMonth, int iDay)
    {
        if ((DateTime.MinValue.Year > iYear) || (iYear > DateTime.MaxValue.Year))
        {
            return false;
        }

        if ((DateTime.MinValue.Month > iMonth) || (iMonth > DateTime.MaxValue.Month))
        {
            return false;
        }

        int iLastDay = DateTime.DaysInMonth(iYear, iMonth);

        if ((DateTime.MinValue.Day > iDay) || (iDay > iLastDay))
        {
            return false;
        }

        return true;
    }

    /// <summary>
    /// 和暦正式文字で表示される日付文字列を取得します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static string GetJpDateStr(DateTime dt)
    {
        var year = GetEraJpYear(dt);
        if (year == string.Empty) return string.Empty;
        return year + "年" + dt.Month.ToString("00") + "月" + dt.Day.ToString("00") + "日";
    }

    /// <summary>
    /// 和暦頭文字で表示される日付文字列を取得します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static string GetShortJpDateStr(DateTime dt)
    {
        var year = GetShortEraJpYear(dt);
        if (year == string.Empty) return string.Empty;
        return year + "/" + dt.Month.ToString("00") + "/" + dt.Day.ToString("00");
    }

    /// <summary>
    /// 昭和=3としたJYYMMDD形式のInt型で日付を取得します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static int GetIntJpDateWithEraNumber(DateTime dt)
    {
        var year = GetEraNumberYear(dt);
        if (year == 0) return 0;
        return year * 10000 + dt.Month * 100 + dt.Day;
    }


    /// <summary>
    /// コードまたは番号から、年号を返します。なかった場合空白が返ります。
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static string GetEraName(string codeStr)
    {
        codeStr = codeStr.ToUpper();
        int code;
        foreach (var item in EraList)
        {
            if (codeStr.Contains(item.Initial)) return item.Name;
            if (int.TryParse(codeStr, out code)) if (code == item.Number) return item.Name;
        }
        return string.Empty;
    }

    /// <summary>
    /// 4月から始まる、西暦の「年度」を取得します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static int GetTheYear(this DateTime dt)
    {
        int year = dt.Year;
        if (dt.Month < 4) year--;
        return year;
    }

    /// <summary>
    /// 4月から始まる、西暦の「年度」の下2桁を取得します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static int GetTheYear2(this DateTime dt)
    {
        int year = dt.Year;
        if (dt.Month < 4) year--;
        year %= 100;
        return year;
    }

    /// <summary>
    /// 数字8ケタからなる西暦をDateTimeに変換します。
    /// </summary>
    /// <param name="dateInt8">8桁のint</param>
    /// <returns></returns>
    public static DateTime ToDateTime(this int dateInt8)
    {
        int year = dateInt8 / 10000;
        int month = (dateInt8 % 10000) / 100;
        int day = dateInt8 % 100;
        if (!IsDate(year, month, day)) return DateTimeNull;
        return new DateTime(year, month, day);
    }

    /// <summary>
    /// 数字6ケタからなる西暦(yyyymm)をDateTimeに変換します。
    /// </summary>
    /// <param name="dateInt8">8桁のint</param>
    /// <returns></returns>
    public static DateTime ToDateTime6(this int dateInt6)
    {
        int year = dateInt6 / 100;
        int month = dateInt6 % 100;
        int day = 1;
        if (!IsDate(year, month, day)) return DateTimeNull;
        return new DateTime(year, month, day);
    }

    /// <summary>
    /// 数字8文字からなる西暦をDateTimeに変換します。失敗した場合、DateTimeNullが返ります。
    /// </summary>
    /// <param name="dateInt8"></param>
    /// <returns></returns>
    public static DateTime ToDateTime(this string dateStr)
    {
        int dateInt8;
        if (dateStr == null) return DateTimeNull;
        if (!int.TryParse(dateStr, out dateInt8)) return DateTimeNull;

        return ToDateTime(dateInt8);
    }

    /// <summary>
    /// 数字6文字からなる西暦(yyyymm)をDateTimeに変換します。失敗した場合、DateTimeNullが返ります。
    /// </summary>
    /// <param name="dateInt8"></param>
    /// <returns></returns>
    public static DateTime ToDateTime6(this string dateStr6)
    {
        int dateInt6;
        if (dateStr6 == null) return DateTimeNull;
        if (!int.TryParse(dateStr6, out dateInt6)) return DateTimeNull;

        return ToDateTime6(dateInt6);
    }

    /// <summary>
    /// 数字8ケタのIntに変換します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static int ToInt(this DateTime dt)
    {
        int di = dt.Day;
        di = di + dt.Month * 100;
        di = di + dt.Year * 10000;
        return di;
    }

    /// <summary>
    /// 現在挿入されている日付がNULLかどうかを返します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static bool IsNullDate(this DateTime dt)
    {
        return dt == DateTimeNull;
    }

    public static string ToJDateShortStr(this DateTime dt)
    {
        return GetShortJpDateStr(dt);
    }

    public static string ToJDateStr(this DateTime dt)
    {
        return GetJpDateStr(dt);
    }

    /// <summary>
    /// 和暦頭文字で表示される年月文字列を取得します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static string ToJyearMonthShortStr(this DateTime dt)
    {
        var year = GetShortEraJpYear(dt);
        if (year == string.Empty) return string.Empty;
        return year + "/" + dt.Month.ToString("00");
    }

    /// <summary>
    /// 和暦頭文字で表示される年の文字列を取得します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static string ToJyearShortStr(this DateTime dt)
    {
        var year = GetShortEraJpYear(dt);
        if (year == string.Empty) return string.Empty;
        return year;
    }

    /// <summary>
    /// 和暦で表示される年月文字列を取得します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static string ToJyearMonthStr(this DateTime dt)
    {
        var year = GetEraJpYear(dt);
        if (year == string.Empty) return string.Empty;
        return year + "年" + dt.Month.ToString("00") + "月";
    }

    /// <summary>
    /// 指定された月の1日の日付を返します。
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static DateTime GetMonthFirstDate(this DateTime dt)
    {
        return new DateTime(dt.Year, dt.Month, 1);
    }

    /// <summary>
    /// 半角にコンバートします。
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static string KanaToHalf(string str)
    {
        return Strings.StrConv(str, VbStrConv.Narrow);
    }



   
    /// <summary>
    /// 平成年から西暦年を取得します
    /// </summary>
    /// <param name="henseiYear"></param>
    /// <returns></returns>
    //public static int GetAdYeaFromH(int henseiYear)
    //{
    //    if (henseiYear < 1 || 70 < henseiYear) throw new Exception("平成から西暦へ変換できない数値が指定されました。( y < 0 || 70 < y )");
    //    var h = EraList.First(e => e.Initial == "H");
    //    return henseiYear += h.Difference;
    //}




    /// <summary>
    /// 和暦から西暦年を取得します
    /// </summary>
    /// <param name="warekiYear">和暦年yy</param>
    /// <param name="warekiMonth">月</param>
    /// <returns></returns>
    public static int GetAdYearFromWarekiYM(int warekiYear,int warekiMonth)
    {

        //20190627182626 furukawa st ////////////////////////
        //明確に変換する。できないものは０
        


        try
        {

            int warekiYM = warekiYear * 100 + warekiMonth;

            if (warekiYM < 104 || 6401 < warekiYM)
                throw new Exception("和暦から西暦へ変換できない数値が指定されました。( ym < 104 || 6401 < ym )  " + warekiYM);
            
            //20190716223609 furukawa st ////////////////////////
            //配列クリア

            EraList.Clear();
            //20190716223609 furukawa ed ////////////////////////

            GetEraList();

            if (warekiYM <= 3104 && warekiYM >= 2001)
            {
                var er = EraList.First(e => e.Initial == "H");
                return warekiYear += er.Difference;
            }
            else if (warekiYM >= 105 && warekiYM <= 1912)
            {
                var er = EraList.First(e => e.Initial == "R");
                return warekiYear += er.Difference;
            }
            else if (warekiYM > 3104)
            {
                var er = EraList.First(e => e.Initial == "S");
                return warekiYear += er.Difference;
            }
            else if (warekiYM <= 104)
            {
                var er = EraList.First(e => e.Initial == "R");
                return warekiYear += er.Difference;
            }

        }
        catch(Exception ex)
        {
            ErrorMessage = ex.Message;
        }




        //int warekiYM = warekiYear * 100 + warekiMonth;

        //DateTime dt = new DateTime(GetAdYearFromWarekiYM(warekiYear, warekiMonth), warekiMonth, 1);

        //if (warekiYM < 104 || 6401 < warekiYM)
        //    throw new Exception("和暦から西暦へ変換できない数値が指定されました。( ym < 104 || 6401 < ym )  " + warekiYM);

        //foreach (var e in EraList)
        //{
        //    if (e.SDate <= dt && dt <= e.EDate ) return warekiYear += e.Difference;
        //}
        
        //20190627182626 furukawa ed ////////////////////////

        return 0;

    }



    //20190619104228 furukawa st ////////////////////////
    //引数変更により削除

    /// <summary>
    /// 西暦年から平成年を取得します
    /// </summary>
    /// <param name="adYear"></param>
    /// <returns></returns>
    //public static int GetHYear(int adYear)
    //{
    //    return adYear - EraList.Find(e => e.Number == 4).Difference;
    //}
    //20190619104228 furukawa ed ////////////////////////


    //20190619102140 furukawa st ////////////////////////
    //令和対応

    /// <summary>
    /// 西暦年から和暦年を取得します
    /// </summary>
    /// <param name="adYear">西暦年</param>
    /// <param name="adMonth">月</param>
    /// <returns>和暦年</returns>
    public static int GetWarekiYearFromADYM(int adYear,int adMonth)
    {
        DateTime tmp = new DateTime(adYear, adMonth, 1);

        foreach(var e in EraList)
        {
            if (e.SDate <= tmp && tmp <=e.EDate) return adYear - e.Difference;
        }
        return 0;
        

    }
    //20190619102140 furukawa ed ////////////////////////
}
