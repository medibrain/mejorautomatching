﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace MejorAutoMatching
{
    public static class Settings
    {
        static string fileName = "settings.xml";
        static XDocument xdoc = new XDocument();

        static Settings()
        {
            if (!System.IO.File.Exists(fileName)) create();
            xdoc = XDocument.Load(fileName);
        }

        static void create()
        {
            XDocument xdoc = new XDocument();
            xdoc.Add(new XElement("Settings"));
            xdoc.Save(fileName);
        }

        public static void Save()
        {
            xdoc.Save(fileName);
        }

        public static string DBAddress
        {
            get
            {
                if (xdoc.Element("Settings").Element("DBAddress") == null) return string.Empty;
                return xdoc.Element("Settings").Element("DBAddress").Value;
            }
            set
            {
                xdoc.Element("Settings").SetElementValue("DBAddress", value);
            }
        }

        public static int DBPort
        {
            get
            {
                if (xdoc.Element("Settings").Element("DBPort") == null) return 0;
                return int.Parse(xdoc.Element("Settings").Element("DBPort").Value);
            }
            set
            {
                xdoc.Element("Settings").SetElementValue("DBPort", value);
            }
        }

        public static string DBEncording
        {
            get
            {
                if (xdoc.Element("Settings").Element("DBEncording") == null) return string.Empty;
                return xdoc.Element("Settings").Element("DBEncording").Value;
            }
            set
            {
                xdoc.Element("Settings").SetElementValue("DBEncording", value);
            }
        }
        public static string DBUserName
        {
            get
            {
                if (xdoc.Element("Settings").Element("DBUserName") == null) return string.Empty;
                return xdoc.Element("Settings").Element("DBUserName").Value;
            }
            set
            {
                xdoc.Element("Settings").SetElementValue("DBUserName", value);
            }
        }

        public static string DBPassword
        {
            get
            {
                if (xdoc.Element("Settings").Element("DBPassword") == null) return string.Empty;
                return xdoc.Element("Settings").Element("DBPassword").Value;
            }
            set
            {
                xdoc.Element("Settings").SetElementValue("DBPassword", value);
            }
        }

        //20190719091231 furukawa st ////////////////////////
        //ログ関連設定
        
        public static bool LOG
        {
            get
            {
                if (xdoc.Element("Settings").Element("LOG") == null) return false;
                return bool.Parse(xdoc.Element("Settings").Element("LOG").Value);
            }
            set
            {
                xdoc.Element("Settings").SetElementValue("LOG", value);
            }
        }

        public static int LOGCNT
        {
            get
            {
                if (xdoc.Element("Settings").Element("LOGCNT") == null) return 0;
                return int.Parse(xdoc.Element("Settings").Element("LOGCNT").Value);
            }
            set
            {
                xdoc.Element("Settings").SetElementValue("LOGCNT", value);
            }
        }
        //20190719091231 furukawa ed ////////////////////////


        //20200217145110 furukawa st ////////////////////////
        //進行表示の表示順設定。0=通常、1=最新が上
        
        public static int LogSort
        {
            get
            {
                if (xdoc.Element("Settings").Element("LOGSort") == null) return 0;
                return int.Parse(xdoc.Element("Settings").Element("LOGSort").Value);
            }
            set
            {
                xdoc.Element("Settings").SetElementValue("LOGSort", value);
            }
        }
        //20200217145110 furukawa ed ////////////////////////


        //20200217150002 furukawa st ////////////////////////
        //接続先情報
        
        public static string strConnectionInfo
        {
            get
            {
                return $"DBServerIP:{DBAddress};port:{DBPort}";
            }
            
        }
        //20200217150002 furukawa ed ////////////////////////
    }
}
