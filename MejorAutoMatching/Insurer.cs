﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MejorAutoMatching
{
    class Insurer
    {
        static string xmlFlie = System.Windows.Forms.Application.StartupPath + "\\insurers.xml";

        protected Dictionary<string, List<ReferData>> dic = new Dictionary<string, List<ReferData>>();
        protected DB db;

        public string Name { get; set; } = string.Empty;
        public string DbName { get; set; } = string.Empty;

        //20200221113542 furukawa st ////////////////////////
        //保険者別接続先を管理するためサーバIP、ポートを追加
        
        public string ServerIP { get; set; } = string.Empty;
        public int DBPort { get; set; } = 0;
        //20200221113542 furukawa ed ////////////////////////

        //20200221134804 furukawa st ////////////////////////
        //固定値排除のためDBuser、password追加

        public string DBUser { get; set; } = string.Empty;
        public string DBPass { get; set; } = string.Empty;
        //20200221134804 furukawa ed ////////////////////////

        protected int targetCYM = 0;
        protected DateTime? startDT = null;


        //20190619102351 furukawa st ////////////////////////
        //令和対応にて関数置換
        
        public string TargetHYM => targetCYM == 0 ? null :
            (DateTimeEx.GetWarekiYearFromADYM(targetCYM / 100, targetCYM % 100)).ToString("00") + "/" + (targetCYM % 100).ToString("00");


                    //public string TargetHYM => targetCYM == 0 ? null :
                    //    (DateTimeEx.GetHYear(targetCYM / 100)).ToString("00") + "/" + (targetCYM % 100).ToString("00");
        //20190619102351 furukawa ed ////////////////////////




        public string StartDT => startDT == null ? string.Empty : startDT.ToString();
        public string LastCheckDT { get; private set; } = string.Empty;

        /// <summary>
        /// 保険者一覧をXMLファイルから読み込みます
        /// </summary>
        /// <param name="mf"></param>
        /// <returns></returns>
        public static List<Insurer> LoadInsurers(MainForm mf)
        {
            var xdoc = XDocument.Load(xmlFlie);
            var l = new List<Insurer>();
            var xins = xdoc.Element("Insurers").Elements("Insurer");

            foreach (var item in xins)
            {
                try
                {
                    if (item.Element("Enable").Value != "true") continue;

                    Insurer ins;
                    var type = item.Element("ClassType").Value;
                    if (type == "OsakaKoiki") ins = new OsakaKoiki();
                    else if (type == "HiroshimaKoiki") ins = new HiroshimaKoiki();
                    else if (type == "NaraKoiki") ins = new NaraKoiki();
                    else if (type == "TokushimaKoiki") ins = new TokushimaKoiki();
                    else if (type == "OtsuKokuho") ins = new OtsuKokuho();
                    else continue;

                    ins.Name = item.Element("Name").Value;
                    ins.DbName = item.Element("DbName").Value;

                    //20200221113751 furukawa st ////////////////////////
                    //保険者別接続先を管理するためサーバIP、ポートを追加
                    
                    ins.ServerIP = item.Element("ServerIP").Value;
                    ins.DBPort = int.Parse(item.Element("DBPort").Value);


                    //20200221113751 furukawa ed ////////////////////////

                    //20200221135436 furukawa st ////////////////////////
                    //固定値排除のためDBuser、password追加
                    
                    ins.DBUser = item.Element("DBUser").Value;
                    ins.DBPass = item.Element("DBPass").Value;
                    //20200221135436 furukawa ed ////////////////////////
                    l.Add(ins);
                }
                catch (Exception ex)
                {
                    Log.ErrorWrite(ex);
                    mf.LogPrint("保険者の読み込みに失敗しました");
                    continue;
                }
            }

            return l;
        }

        /// <summary>
        /// 指定された年月の提供データをプールし、マッチング監視を開始します
        /// </summary>
        /// <param name="yyyymm">西暦年月</param>
        /// <param name="mf">メインフォーム</param>
        /// <param name="ins">insurerクラス</param>
        /// <returns></returns>
        /// 

        //20200221121600 furukawa st ////////////////////////
        //保険者別接続先を管理するため、保険者の設定値を使用する
        
        public bool Start(int yyyymm, MainForm mf,Insurer ins)
                    //public bool Start(int yyyymm, MainForm mf)
        //20200221121600 furukawa ed ////////////////////////
        {
            targetCYM = yyyymm;
            mf.LogPrint(Name + " " + TargetHYM + "のデータ取得を開始します");


            //20200221121655 furukawa st ////////////////////////
            //保険者別接続先を管理するため、保険者の設定値をコネクションに使用する

            if (db == null) db = new DB(ins.ServerIP, ins.DBPort, ins.DbName, ins.DBUser, ins.DBPass);

            //if (db == null) db = new DB(Settings.DBAddress, Settings.DBPort,
            //       this.DbName, Settings.DBUserName, Settings.DBPassword);
            //20200221121655 furukawa ed ////////////////////////


            if (getProvidedDatas())
            {
                if (targetCYM == 0)
                {
                    //データ取得中に停止作業がされた場合
                    dic.Clear();
                    targetCYM = 0;
                    startDT = null;
                    return false;
                }

                startDT = DateTime.Now;
                mf.LogPrint(Name + " " + dic.Count.ToString() + "件の被保険者データを取得しました");
                mf.LogPrint(Name + " 監視を開始しました");
                return true;
            }
            else
            {
                dic.Clear();
                targetCYM = 0;
                startDT = null;
                mf.LogPrint(Name + " データ取得に失敗しました");
                return false;
            }
        }

        /// <summary>
        /// マッチング監視を停止します
        /// </summary>
        public void Stop(MainForm mf)
        {
            startDT = null;
            targetCYM = 0;
            dic.Clear();
            mf.LogPrint(Name + " 監視を中止しました");
        }

        /// <summary>
        /// マッチングを行います
        /// </summary>
        /// <param name="mf"></param>
        public void Matching(MainForm mf)
        {
            if (startDT == null) return;

            var sid = getNeedMatchingId();
            if (sid == 0)
            {
                LastCheckDT = DateTime.Now.ToString("MM/dd HH:mm:ss");
                return;
            }

            mf.LogPrint(Name + " スキャンID:" + sid.ToString() + " のOCR突合を開始します");
            var l = AppOcr.GetOcr(db, sid);
            int t = 0;


            //各Appを突合
            foreach (var item in l)
            {
                var pd = getMatchData(item);
                if (pd == null) continue;

                t++;
                update(item, pd);
            }

            //マッチング処理の終了を記録
            var p = new Dapper.DynamicParameters();
            p.Add("sid", sid, System.Data.DbType.Int32);
            db.Excute("INSERT INTO ocr_ordered(sid) VALUES (@sid);", p);

            if (l.Count == 0)
            {
                mf.LogPrint(Name + " スキャンID:" + sid.ToString() + " のOCR突合が完了しました  対象データはありませんでした");
            }
            else
            {
                var f = l.Count - t;
                mf.LogPrint(Name + " スキャンID:" + sid.ToString() + " のOCR突合が完了しました  " +
                    "        成功:" + t.ToString() + "  失敗:" + f.ToString() + "  突合率:" + ((t * 100) / (l.Count)).ToString() + "％");
            }

            LastCheckDT = DateTime.Now.ToString("MM/dd HH:mm:ss");
        }

        /// <summary>
        /// OCRのマッチングが必要なスキャングループがあるかチェックし、一番若いIDを返します。
        /// 対象となるIDがなかった場合、0が返ります。
        /// </summary>
        /// <returns></returns>
        private int getNeedMatchingId()
        {
            var sql = "SELECT s.sid FROM scan AS s " +
                "LEFT OUTER JOIN ocr_ordered AS o ON(s.sid=o.sid) " +
                "WHERE s.status=@st AND o.sid IS NULL " +
                "AND s.cyear=@y AND s.cmonth=@m " +
                "ORDER BY s.sid LIMIT 1;";

            //20190619102526 furukawa st ////////////////////////
            //令和対応にて関数置換
            
                        //int hy = DateTimeEx.GetHYear(targetCYM / 100);
            int m = targetCYM % 100;
            int hy = DateTimeEx.GetWarekiYearFromADYM(targetCYM / 100,m);
            //20190619102526 furukawa ed ////////////////////////


            var p = new Dapper.DynamicParameters();
            p.Add("st", (int)AppOcr.SCAN_OCR_FINSHED, System.Data.DbType.Int32);
            p.Add("y", hy, System.Data.DbType.Int32);
            p.Add("m", m, System.Data.DbType.Int32);

             
            Log.WriteInfoLog("getNeedMatchingId,sql=" + sql + 
                "st:" + (int)AppOcr.SCAN_OCR_FINSHED + " y:" + hy +" m:" + m);

            var l = db.Query<int>(sql, p);
            return l.FirstOrDefault();
        }

        /// <summary>
        /// 指定されたDicから条件に合うデータを返します
        /// なかった場合、または複数個あった場合はnullを返します
        /// </summary>
        /// <param name="dic">被保険者番号をキーとしたDictionary</param>
        /// <param name="mediYM"></param>
        /// <param name="num"></param>
        /// <param name="drNum"></param>
        /// <param name="total"></param>
        /// <returns></returns>
        protected virtual ReferData getMatchData(AppOcr app)
        {
            return null;
        }

        /// <summary>
        /// 指定された月の提供データを読み込みます
        /// </summary>
        /// <returns></returns>
        protected virtual bool getProvidedDatas()
        {
            return false;
        }

        private bool update(AppOcr app, ReferData pd)
        {
            var sql = "UPDATE application SET " +
                $"ayear=@ay, amonth=@am, hnum=@hn, atotal=@at, " +
                $"ym=@ym, statusflags=statusflags|{(int)STATUS_FLAG.自動マッチ済}, " +
                $"rrid=@rrid " +
                "WHERE aid=@aid;";

            var p = new Dapper.DynamicParameters();

            //20190619102626 furukawa st ////////////////////////
            //令和対応にて関数置換
            
            p.Add("ay", DateTimeEx.GetWarekiYearFromADYM(pd.MediYM / 100,pd.MediYM % 100), System.Data.DbType.Int32);
                        //p.Add("ay", DateTimeEx.GetHYear(pd.MediYM / 100), System.Data.DbType.Int32);

            //20190619102626 furukawa ed ////////////////////////

            p.Add("am", pd.MediYM % 100, System.Data.DbType.Int32);
            p.Add("hn", pd.Num, System.Data.DbType.String);
            p.Add("at", pd.Total, System.Data.DbType.Int32);
            p.Add("ym", pd.MediYM, System.Data.DbType.Int32);
            p.Add("rrid", pd.RrID, System.Data.DbType.Int32);
            p.Add("aid", app.AID, System.Data.DbType.Int32);

            using (var tran = db.CreateTran())
            {
                if (db.Excute(sql, p, tran) && pd.SetAid(db, app.AID, tran))
                {
                    tran.Commit();
                    return true;
                }
                else
                {
                    tran.Rollback();
                    return false;
                }
            }
        }
    }
}
